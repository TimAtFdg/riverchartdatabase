﻿CREATE TABLE [dbo].[PETypes] (
    [PEType]           NCHAR (2)      NOT NULL,
    [PETypeDefinition] NCHAR (100)    NULL,
    [Using]            BIT            NULL,
    [Notes]            NVARCHAR (250) NULL,
    CONSTRAINT [PK_PETypes] PRIMARY KEY CLUSTERED ([PEType] ASC)
);

