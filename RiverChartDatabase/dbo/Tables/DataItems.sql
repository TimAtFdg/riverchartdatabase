﻿CREATE TABLE [dbo].[DataItems] (
    [dataItemID]                  INT           IDENTITY (1, 1) NOT NULL,
    [name]                        NCHAR (150)   NOT NULL,
    [PEType]                      NCHAR (2)     NOT NULL,
    [stationID]                   NCHAR (5)     NOT NULL,
    [hasStageData]                BIT           NULL,
    [hasDischargeData]            BIT           NULL,
    [statusMessageDateTime]       DATETIME      NULL,
    [statusMessage]               VARCHAR (500) NULL,
    [modificationMessageDateTime] DATETIME      NULL,
    [modificationMessage]         VARCHAR (500) NULL,
    [primaryDataValue]            FLOAT (53)    NULL,
    [secondaryDataValue]          FLOAT (53)    NULL,
    [dataDateTime]                DATETIME      NULL,
    [dataDateTimeLabel]           NCHAR (5)     NULL,
    [currentCount]                BIGINT        NULL,
    [lastCount]                   BIGINT        NULL,
    CONSTRAINT [PK_DataItems] PRIMARY KEY CLUSTERED ([dataItemID] ASC),
    CONSTRAINT [FK_DataItems_PETypes] FOREIGN KEY ([PEType]) REFERENCES [dbo].[PETypes] ([PEType]),
    CONSTRAINT [FK_DataItems_StationInformation] FOREIGN KEY ([stationID]) REFERENCES [dbo].[StationInformation] ([stationID])
);

