﻿CREATE TABLE [dbo].[DSTCodes] (
    [DSTCode]     INT        NOT NULL,
    [Description] NCHAR (50) NOT NULL,
    CONSTRAINT [PK_DSTCodes] PRIMARY KEY CLUSTERED ([DSTCode] ASC)
);

