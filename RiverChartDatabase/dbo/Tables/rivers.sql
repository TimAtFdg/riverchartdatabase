﻿CREATE TABLE [dbo].[rivers] (
    [riverID]     INT            IDENTITY (1, 1) NOT NULL,
    [stationID]   NCHAR (5)      NOT NULL,
    [name1]       NVARCHAR (50)  NULL,
    [name2]       NVARCHAR (50)  NULL,
    [displayName] NVARCHAR (100) NULL,
    CONSTRAINT [PK_rivers] PRIMARY KEY CLUSTERED ([riverID] ASC)
);

