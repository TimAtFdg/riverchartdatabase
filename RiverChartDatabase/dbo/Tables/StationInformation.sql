﻿CREATE TABLE [dbo].[StationInformation] (
    [stationID]                      NCHAR (5)      NOT NULL,
    [name1]                          NCHAR (50)     NULL,
    [name2]                          NCHAR (50)     NULL,
    [displayName]                    NVARCHAR (100) NULL,
    [latitude]                       FLOAT (53)     NULL,
    [longitude]                      FLOAT (53)     NULL,
    [county]                         NCHAR (50)     NULL,
    [state]                          NCHAR (50)     NULL,
    [elevation]                      INT            NULL,
    [description]                    NCHAR (100)    NULL,
    [UTCTimeOffset]                  INT            NOT NULL,
    [observesDaylightTime]           BIT            NOT NULL,
    [StationInformationCodeID]       INT            NULL,
    [StationInformationCodeDateTime] DATETIME       NULL,
    CONSTRAINT [PK_StationInformation] PRIMARY KEY CLUSTERED ([stationID] ASC),
    CONSTRAINT [FK_StationInformation_StationInformationCodes] FOREIGN KEY ([StationInformationCodeID]) REFERENCES [dbo].[StationInformationCodes] ([StationInformationCodeID])
);

