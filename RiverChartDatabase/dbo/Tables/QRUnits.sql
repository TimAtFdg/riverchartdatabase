﻿CREATE TABLE [dbo].[QRUnits] (
    [QRUnitID]       INT        IDENTITY (1, 1) NOT NULL,
    [riverID]        INT        NOT NULL,
    [StageUnits]     NCHAR (50) NULL,
    [DischargeUnits] NCHAR (50) NULL,
    [stationID]      NCHAR (5)  NOT NULL,
    CONSTRAINT [PK_QRUnits] PRIMARY KEY CLUSTERED ([QRUnitID] ASC),
    CONSTRAINT [FK_QRUnits_rivers] FOREIGN KEY ([riverID]) REFERENCES [dbo].[rivers] ([riverID])
);

