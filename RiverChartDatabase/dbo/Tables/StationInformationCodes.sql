﻿CREATE TABLE [dbo].[StationInformationCodes] (
    [StationInformationCodeID] INT        NOT NULL,
    [Description]              NCHAR (10) NULL,
    CONSTRAINT [PK_StationInformationCode] PRIMARY KEY CLUSTERED ([StationInformationCodeID] ASC)
);

