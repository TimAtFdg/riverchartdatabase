﻿CREATE TABLE [dbo].[DataErrorLog] (
    [stationID]         NCHAR (5)     NOT NULL,
    [PEType]            NCHAR (2)     NOT NULL,
    [errorTypeId]       INT           NOT NULL,
    [errorMessage]      VARCHAR (250) NOT NULL,
    [timeOfObservation] DATETIME      NULL,
    [dataErrorLogId]    INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_DataErrorLog] PRIMARY KEY CLUSTERED ([dataErrorLogId] ASC)
);


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER dbo.trgAfterInsert
   ON dbo.DataErrorLog
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.DataErrorLog
	set timeOfObservation = GETDATE()
	from inserted

END