﻿CREATE TABLE [dbo].[DataItemObservation] (
    [stationID]             NCHAR (5)     NOT NULL,
    [PEType]                NCHAR (2)     NOT NULL,
    [isStageData]           BIT           NOT NULL,
    [isDischargeData]       BIT           NOT NULL,
    [timeAsString]          NCHAR (35)    NOT NULL,
    [time]                  DATETIME2 (7) NOT NULL,
    [value]                 FLOAT (53)    NOT NULL,
    [DataItemObservationId] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_DataItemObservation_1] PRIMARY KEY CLUSTERED ([DataItemObservationId] ASC)
);

