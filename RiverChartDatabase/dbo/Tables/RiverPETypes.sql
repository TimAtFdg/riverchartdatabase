﻿CREATE TABLE [dbo].[RiverPETypes] (
    [RiverPETypeID] INT       IDENTITY (1, 1) NOT NULL,
    [riverID]       INT       NOT NULL,
    [PEType]        NCHAR (2) NOT NULL,
    CONSTRAINT [PK_RiverPETypes] PRIMARY KEY CLUSTERED ([RiverPETypeID] ASC),
    CONSTRAINT [FK_RiverPETypes_rivers] FOREIGN KEY ([riverID]) REFERENCES [dbo].[rivers] ([riverID])
);

