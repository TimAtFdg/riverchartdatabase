﻿CREATE TABLE [dbo].[HGUnits] (
    [HGUnitID]       INT        IDENTITY (1, 1) NOT NULL,
    [riverID]        INT        NOT NULL,
    [StageUnits]     NCHAR (50) NULL,
    [DischargeUnits] NCHAR (50) NULL,
    [stationID]      NCHAR (5)  NULL,
    CONSTRAINT [PK_HGUnits] PRIMARY KEY CLUSTERED ([HGUnitID] ASC),
    CONSTRAINT [FK_HGUnits_rivers] FOREIGN KEY ([riverID]) REFERENCES [dbo].[rivers] ([riverID])
);

